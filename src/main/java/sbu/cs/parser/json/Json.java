package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.Arrays;

import sbu.cs.parser.json.num.Num;
import sbu.cs.parser.json.pair.Pairs;

public class Json implements JsonInterface {
	@Override
	public String toString() {
		return  Arrays.toString(chData);
	}

	/**
	 * this function will return value of key in String data-type
	 * including Num , Json , Boolean , ArrayList , null
	 */
	@Override
	public String getStringValue(String key) {
		String stringValue;
		try {
			stringValue = pairs.getValue(key).toString();
		}
		catch (Exception e) {
			System.out.println("Parse error happened :getStringValue"+e);
			stringValue="null";
		}
		return stringValue;
	}

	private final Pairs pairs;
	private final char[] chData;
	private int cursor = 0;
	private boolean end = false;
	private String strData;
	private final String numShow = "0123456789.";

	/**
	 * getter
	 */
	public int getCursor() {
		return cursor;
	}

	/**
	 * get a JSON String ,save and then parse it by calling parsIt()
	 */
	public Json(String strData) {
		pairs = new Pairs();
		this.strData = strData;
		this.chData = strData.toCharArray();
		try {
			parseIt();
		}
		catch (Exception e){
			System.out.println("Parsing error happened :JsonParser.parse(String data):new Json(String strData):"+e);
			e.printStackTrace();
		}
	}

	/**
	 * this function will parse JSON 
	 * it will prevent saving strData more than after the "}" which ended this JSON
	 * (strData used as toString result)
	 */
	private void parseIt() throws Exception {
		findBeginIndex();
		if (cursor == chData.length)
			throw new Exception("Ended unexpectedly :cursor at" + cursor+"out of bounds(end element+1)");
		while (!end) {
//			System.out.println("findPair()");
			findPair();
		}
		if (1+cursor<strData.length()) {
			strData=strData.substring(0, cursor);
		}
	}

	/**
	 * this function will find first "{" that begins JSON key-value pairs
	 */
	private void findBeginIndex() {
		while (cursor < chData.length && chData[cursor] != '{') {
			cursor++;
		}
	}

	/**
	 * this function will find our JSON key-value pairs or if there is not any more
	 * pair will find "}" and make end=true
	 */
	private void findPair() throws Exception {
		String key = findKey();
//		System.out.println("key:"+key);
		if (key!=null) {
			Object value = findValue();
			ignoreTillNext('}');
//			System.out.println("value:"+value);
			pairs.add(key, value);
//			System.out.println (pairs.getValue(key));
		}
	}

	/**
	 * this function will find a string key or "}" and make end=true
	 */
	private String findKey() {
//		System.out.println("findKeyStarts" );
		while (cursor < chData.length ) {
			if(chData[cursor] == '\"'){
				break;
			}
			if (chData[cursor] == '}') {
//				System.out.println("end=true"+chData[cursor]);
				end = true;
				return null;
			}
//			else {
//				System.out.println("end=false"+chData[cursor] );
//			}
			cursor++;
		}
//		System.out.println(chData[cursor-1]);
		return extractString();
	}

	/**
	 * this function will find a value
	 * 
	 * @return Object that can be Array , Json , number , null, boolean
	 */
	private Object findValue() throws Exception {
		while (cursor < chData.length && chData[cursor] != ':') {
			cursor++;
		}
		cursor++;
		while (cursor < chData.length) {
			switch (chData[cursor]) {
			case ' ':
				cursor++;
				break;
			case '\"':
				return extractString();
			case '[':
				return extractArray();
			case '{':
				return extractJson();
			case ',':
				return null;
			case '}':
				end = true;
				return null;
			case 'n':
				extractNull();
				return null;
			case 'f':
				return extractFalse();
			case 't':
				return extractTrue();
			default:
				return findNum();
			}
		}
		throw new Exception("Unexpected end :cursor at" + cursor+"->"+chData[cursor]);
	}

	/*
	 * this function will extract a string
	 */
	private String extractString() {
		int startPoint = ++cursor;
//		System.out.println("start"+cursor+chData[cursor]);
//		System.out.println(cursor +" "+chData.length);
		while (cursor < chData.length) {
			if (chData[cursor] == '\"') {
				if (chData[cursor - 1] != '\\') {
					break;
				}
			}
			cursor++;
		}
		return strData.substring(startPoint, cursor);
	}


	/**
	 * this function will extract a JSON
	 */

	private Json extractJson() /*throws Exception*/ {
		String jsonStr = strData.substring(cursor);
		Json nestedJson = new Json(jsonStr);
		cursor += nestedJson.cursor;
		return nestedJson;
	}

	/*
	 * this function will extract null
	 */
	private void extractNull() throws Exception {
		if (chData[cursor + 1] == 'u' 
				&& chData[cursor + 2] == 'l' 
				&& chData[cursor + 3] == 'l') {
			cursor += 3;
		} else
			throw new Exception("Expected null :cursor at" + cursor+"->"+chData[cursor]);

	}

	/*
	 * this function will extract false
	 */
	private Boolean extractFalse() throws Exception {
		if (chData[cursor + 1] == 'a' 
				&& chData[cursor + 2] == 'l' 
				&& chData[cursor + 3] == 's'
				&& chData[cursor + 4] == 'e') {
			cursor += 4;
			return false;
		} else
			throw new Exception("Expected false :cursor at" + cursor+"->"+chData[cursor]);
	}

	/*
	 * this function will extract true
	 */
	private Boolean extractTrue() throws Exception {
		if (chData[cursor + 1] == 'r'
				&& chData[cursor + 2] == 'u' 
				&& chData[cursor + 3] == 'e') {
			cursor += 4;
			return true;
		} else
			throw new Exception("Expected true :cursor at" + cursor+"->"+chData[cursor]);
	}

	/*
	 * this function will extract an ArrayList
	 */
	private ArrayList<Object> extractArray() throws Exception {
		ArrayList<Object> arr=new ArrayList<>();
		int startPoint = cursor++;
		while (cursor < chData.length) {
			switch (chData[cursor]) {
			case ' ':
				cursor++;
				break;
			case ']':
				return arr;
			case '\"':
				while (cursor < chData.length && chData[cursor]!=']') {
					try {
						startPoint = cursor;
						arr.add(extractString());
						ignoreTillNext();
					}
					catch(Exception e){
						cursor=startPoint;
						nullHandler(e,arr);
					}
				}
				return arr;
			case '[':
				while (cursor < chData.length && chData[cursor]!=']') {
					try {
						startPoint = cursor;
						arr.add(extractArray());
						ignoreTillNext();
					}
					catch(Exception e){
						cursor=startPoint;
						nullHandler(e,arr);
					}
				}
				return arr;
			case '{':
				while (cursor < chData.length && chData[cursor]!=']') {
					try {
						startPoint = cursor;
						arr.add(extractJson());
						ignoreTillNext();
					}
					catch(Exception e){
						cursor=startPoint;
						nullHandler(e,arr);
					}
				}
				return arr;
			case 'n':
				extractNull();
				ignoreTillNext();
				arr.add(null);
				break;
			case ',':
				arr.add(null);
				cursor++;
				break;
			case '\'':
				while (cursor < chData.length && chData[cursor]!=']') {
					try {
						startPoint = cursor;
						arr.add(extractChar());
						ignoreTillNext();
					}
					catch(Exception e){
						cursor=startPoint;
						nullHandler(e,arr);
					}
				}
				return arr;
			case 'f':
			case 't':
				while (cursor < chData.length && chData[cursor]!=']') {
					try {
						startPoint = cursor;
						arr.add(extractBoolean());
						ignoreTillNext();
					}
					catch(Exception e){
						cursor=startPoint;
						nullHandler(e,arr);
					}
				}
				return arr;
			default:
				while (cursor < chData.length && chData[cursor]!=']') {
					try {
						startPoint = cursor;
						arr.add(findNum());
						ignoreTillNext();
					}
					catch(Exception e){
						cursor=startPoint;
						nullHandler(e,arr);
					}
				}
			}
		}
		throw new Exception("Expected simple defined array :cursor at" + cursor+" out of bounds");
	}
	/**
	 * this function will check if the occurred error was caused by null element
	 * if yes , it will add a null element to ArrayList else throws Exceptions
	 */
	private void nullHandler(Exception e, ArrayList<Object> arr) throws Exception {
		if (findNull()) {
			arr.add(null);
		}
		else {
			throw new Exception (e+"\n nullHandler could not handle it as null \""+chData[cursor]+"\"");
		}
	}

	/**
	 * this function will ignore everything till next element that is separated by "," 
	 * if there is no more element so it will be stopped
	 */
	private void ignoreTillNext() {
		while (cursor < chData.length && chData[cursor]!=',' && chData[cursor]!=']') {
			cursor++;
		}
		if (cursor < chData.length && chData[cursor]!=']')
			cursor++;
	}

	/**
	 * this function will ignore everything till next element that is separated by ","
	 * if there is no more element so it will be stopped
	 * overloaded
	 */
	private void ignoreTillNext(char ch) {
		while (cursor < chData.length && chData[cursor]!=',' && chData[cursor]!=ch) {
			cursor++;
		}
		if (cursor < chData.length && chData[cursor]!=ch)
			cursor++;
	}
	/**
	 * this function will check if there was any null
	 */
	private boolean findNull() throws Exception {
		while (true) {
			switch (chData[cursor]) {
			case ' ':
				cursor++;
				break;
			case 'n':
				extractNull();
				ignoreTillNext();
				return true;
			case ',':
				cursor++;
				return true;
			default :
				return false;
			}
		}
	}

	/**
	 * this function will find and return a Character 
	 */
	private Character extractChar() throws Exception {
		ignoreSpace();
		if (chData[cursor]=='\'') {
			cursor++;
			return chData[cursor];
		}
		throw new Exception("Expected simple defined character :cursor at" + cursor+"->"+chData[cursor]);
	}
	
	private Boolean extractBoolean() throws Exception {
		if (chData[cursor]=='f') {
			return extractFalse();
		}
		return extractTrue();
	}

	/**
	 * this function will find a number
	 */
	private Num findNum() throws Exception{
		ignoreSpace();
		if (  numShow.indexOf(chData[cursor])==-1) {
			throw new Exception("Expected Num : cursor at : "+cursor+"->"+chData[cursor]);
		}
		int startPoint = cursor;
		while (cursor < chData.length) {
			if (  numShow.indexOf(chData[cursor])==-1) {
				break;
			}
			cursor++;
		}
		return new Num(strData.substring(startPoint, --cursor+1));
	}

	private void ignoreSpace() {
		while (cursor < chData.length && chData[cursor]==' ') {
			cursor++;
		}
	}

	/**
	 * this function will return value of key in Object data type that can be casted
	 * to it's real data-type
	 */
	public Object getValue(String key) {
		return pairs.getValue(key);
	}
}
