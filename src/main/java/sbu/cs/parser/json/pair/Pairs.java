package sbu.cs.parser.json.pair;

import java.util.HashMap;

public class Pairs {
	public Pairs() {
		this.keyValues = new HashMap<>();
	}
	public HashMap<String, Object> keyValues;
	public Object getValue(String key) {	
		return keyValues.get(key);
	}
	public void add(String key, Object value) {
		keyValues.put(key, value);
	}
}
