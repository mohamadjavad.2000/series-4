package sbu.cs.parser.json;

public class JsonParser {

    /**
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {
    	if (data.length()<4)
    		return null;
        return new Json(data);
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
