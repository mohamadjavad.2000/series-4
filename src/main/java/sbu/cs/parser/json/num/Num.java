package sbu.cs.parser.json.num;

/**
 * A data-type that can contain numbers in different shows
 */
public class Num {

	@Override
	public String toString() {
		return  number;
	}

	private String number;
	/**
	 * default
	 */
	public Num() {}
	/**
	 * gets a number and saves it
	 */
	public Num(String number) {
		this.setNumber(number);
	}

	/**
	 * getter
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * setter
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	
}
